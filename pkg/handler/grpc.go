// Copyright © 2017 The Qaclana Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package handler

import (
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/qaclana/qaclana-backend/pkg/proto"
	"gitlab.com/qaclana/qaclana-backend/pkg/sysstate"
)

type QaclanaGrpcBackend struct{}

func (s *QaclanaGrpcBackend) Receive(in *backend.Empty, stream backend.SystemStateService_ReceiveServer) error {
	log.Printf("Got a listener to send updates to: %s", stream)

	// sending the current state:
	current, _ := sysstate.Current()
	stream.Send(&backend.SystemState{State: current})

	c := make(chan backend.State)
	sysstate.ListenForUpdates(c)
	for {
		select {
		case state := <-c:
			stream.Send(&backend.SystemState{State: state})
			log.Println("A system state event has been sent downstream: ", state)
		}
	}
	return nil
}

func RegisterGrpcHandler(s *grpc.Server) {
	backend.RegisterSystemStateServiceServer(s, &QaclanaGrpcBackend{})
	reflection.Register(s)
}
